
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('pages/deadlinetracker.vue')},
      {path: '/deadline', component: () => import('pages/deadlinetracker.vue')},
      {path: '/kanban', component: () => import('pages/kanban.vue')  },
       
    ]
  },

  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
